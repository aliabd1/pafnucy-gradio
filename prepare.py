import numpy as np
import pandas as pd
import h5py

import pybel
from tfbio.data import Featurizer

import os


def input_file(path):
    """Check if input file exists."""

    path = os.path.abspath(path)
    if not os.path.exists(path):
        raise IOError('File %s does not exist.' % path)
    return path


def output_file(path):
    """Check if output file can be created."""

    path = os.path.abspath(path)
    dirname = os.path.dirname(path)

    if not os.access(dirname, os.W_OK):
        raise IOError('File %s cannot be created (check your permissions).'
                      % path)
    return path


def string_bool(s):
    s = s.lower()
    if s in ['true', 't', '1', 'yes', 'y']:
        return True
    elif s in ['false', 'f', '0', 'no', 'n']:
        return False
    else:
        raise IOError('%s cannot be interpreted as a boolean' % s)


class FakeArguments(object):
  def __init__(self):
    self.ligand_format = "smi"
    self.pocket_format = "smi"
    self.output = "data.hdf"
    self.mode = "w"
    self.affinities = None
    self.verbose = False

args = FakeArguments()

# TODO: training set preparation (allow to read affinities)


featurizer = Featurizer()


def get_pocket(pocket_smi):
    try:
        pocket = pybel.readstring('mol2', pocket_smi)
    except:
        raise IOError('Cannot read %s string' % pocket_smi)

    pocket_coords, pocket_features = featurizer.get_features(pocket,
                                                             molcode=-1)
    return pocket_coords, pocket_features


def get_ligand(pocket_smi):
    ligand_file = "ligand.txt"
    name = os.path.splitext(os.path.split(ligand_file)[1])[0]
    try:
        ligand = pybel.readstring(args.ligand_format, pocket_smi)
    except:
        raise IOError('Cannot read %s string' % pocket_smi)

    ligand_coords, ligand_features = featurizer.get_features(ligand,
                                                             molcode=1)
    return name, ligand_coords, ligand_features


def prepare_hdf(ligand_smi, pocket_smi):
    with h5py.File(args.output, args.mode) as f:
        name, ligand_coords, ligand_features = get_ligand(ligand_smi)
        pocket_coords, pocket_features = get_pocket(pocket_smi)

        centroid = ligand_coords.mean(axis=0)
        ligand_coords -= centroid
        pocket_coords -= centroid

        data = np.concatenate(
            (np.concatenate((ligand_coords, pocket_coords)),
             np.concatenate((ligand_features, pocket_features))),
            axis=1,
        )

        # dataset = f.create_dataset(name, data=data, shape=data.shape,
        #                            dtype='float32', compression='lzf')

        return data
