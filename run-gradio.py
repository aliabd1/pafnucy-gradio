import gradio
import prepare
import predict
from pockets import pockets_smi
import matplotlib.pyplot as plt

### Create a prediction function
def affinity(ligand, complex_value):
  # pocket = pockets_smi[complex_value]
  with open("tests/data/complexes/{}/{}_pocket.mol2".format(complex_value, complex_value),
            "r") as m:
        mol2_string = m.read()

  dataset = prepare.prepare_hdf(ligand, mol2_string)
  pred = predict.predict_affinity(dataset)
  image_path = "images/{}.jpeg".format(complex_value)
  image = plt.imread(image_path)
  return pred, image
affinity("CC", "3ui7")

### Create some examples (optional)
complex_choices = [
    "3ui7",
    "3uuo",
    "4llx",
    "4mrw",
    "4mrz",
    "4msc",
    "4msn",
    "5c1w",
    "5c2h",
    "5c28",
    "10gs"
]
examples = []
for complex in complex_choices:
    with open("tests/data/complexes/{}/{}_ligand_smi.txt".format(complex, complex), "r") as s:
        smiles_string = s.read()
    with open("tests/data/complexes/{}/{}_ligand_cml.txt".format(complex, complex), "r") as c:
        cml_string = c.read()
    examples.append([{"smile": smiles_string, "cml": cml_string}, complex])
### Launch the Gradio Interface
io = gradio.Interface(affinity, [gradio.inputs.Molecule(label="Ligand Molecule"),
                                 gradio.inputs.Dropdown(
                                     choices=complex_choices,
                                     label="Protein Pocket (by pdbid)")],
                      [gradio.outputs.Label(label="Binding Affinity ("
                                                  "-logKd/Ki)"),
                       gradio.outputs.Image(
                                          label="Complex Structure")],
                                      examples=examples)
io.launch(share=True)