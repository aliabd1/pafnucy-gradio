<?xml version="1.0"?>
<molecule id="id4llx_ligand" xmlns="http://www.xml-cml.org/schema">
 <atomArray>
  <atom id="a1" elementType="C" x2="1.732051" y2="-0.000000"/>
  <atom id="a2" elementType="C" x2="0.866025" y2="-0.500000"/>
  <atom id="a3" elementType="C" x2="0.866025" y2="-1.500000"/>
  <atom id="a4" elementType="C" x2="0.000000" y2="-2.000000"/>
  <atom id="a5" elementType="C" x2="0.000000" y2="-3.000000"/>
  <atom id="a6" elementType="N" x2="-0.866025" y2="-1.500000"/>
  <atom id="a7" elementType="C" x2="-0.866025" y2="-0.500000"/>
  <atom id="a8" elementType="N" x2="-1.732051" y2="-0.000000"/>
  <atom id="a9" elementType="N" x2="0.000000" y2="-0.000000"/>
  <atom id="a10" elementType="H" x2="2.232051" y2="0.866025"/>
  <atom id="a11" elementType="H" x2="1.232051" y2="0.866025"/>
  <atom id="a12" elementType="H" x2="2.598076" y2="-0.500000"/>
  <atom id="a13" elementType="H" x2="1.732051" y2="-2.000000"/>
  <atom id="a14" elementType="H" x2="0.500000" y2="-3.866025"/>
  <atom id="a15" elementType="H" x2="1.000000" y2="-3.000000"/>
  <atom id="a16" elementType="H" x2="-0.866025" y2="-3.500000"/>
  <atom id="a17" elementType="H" x2="-1.732051" y2="1.000000"/>
  <atom id="a18" elementType="H" x2="-2.598076" y2="-0.500000"/>
 </atomArray>
 <bondArray>
  <bond atomRefs2="a1 a2" order="1"/>
  <bond atomRefs2="a2 a3" order="2"/>
  <bond atomRefs2="a2 a9" order="1"/>
  <bond atomRefs2="a3 a4" order="1"/>
  <bond atomRefs2="a4 a5" order="1"/>
  <bond atomRefs2="a4 a6" order="2"/>
  <bond atomRefs2="a6 a7" order="1"/>
  <bond atomRefs2="a7 a8" order="1"/>
  <bond atomRefs2="a7 a9" order="2"/>
  <bond atomRefs2="a1 a10" order="1"/>
  <bond atomRefs2="a1 a11" order="1"/>
  <bond atomRefs2="a1 a12" order="1"/>
  <bond atomRefs2="a3 a13" order="1"/>
  <bond atomRefs2="a5 a14" order="1"/>
  <bond atomRefs2="a5 a15" order="1"/>
  <bond atomRefs2="a5 a16" order="1"/>
  <bond atomRefs2="a8 a17" order="1"/>
  <bond atomRefs2="a8 a18" order="1"/>
 </bondArray>
</molecule>
