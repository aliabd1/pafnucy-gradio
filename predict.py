import numpy as np
import pandas as pd
import h5py

import tensorflow as tf
from tfbio.data import Featurizer, make_grid

import os


def input_file(path):
    """Check if input file exists."""

    path = os.path.abspath(path)
    if not os.path.exists(path):
        raise IOError('File %s does not exist.' % path)
    return path


def network_prefix(path):
    """Check if all file required to restore the network exists."""

    from glob import glob
    dir_path, file_name = os.path.split(path)
    path = os.path.join(os.path.abspath(dir_path), file_name)

    for extension in ['index', 'meta', 'data*']:
        file_name = '%s.%s' % (path, extension)

        # use glob instead of os because we need to expand the wildcard
        if len(glob(file_name)) == 0:
            raise IOError('File %s does not exist.' % file_name)

    return path


def batch_size(value):
    """Check if batch size is a non-negative integer"""

    value = int(value)
    if value < 0:
        raise ValueError('Batch size must be positive, %s given' % value)
    return value


def output_file(path):
    """Check if output file can be created."""

    path = os.path.abspath(path)
    dirname = os.path.dirname(path)

    if not os.access(dirname, os.W_OK):
        raise IOError('File %s cannot be created (check your permissions).'
                      % path)
    return path


def string_bool(s):
    s = s.lower()
    if s in ['true', 't', '1', 'yes', 'y']:
        return True
    elif s in ['false', 'f', '0', 'no', 'n']:
        return False
    else:
        raise IOError('%s cannot be interpreted as a boolean' % s)


class FakeArguments(object):
  def __init__(self):
    self.input = "data.hdf"
    self.network = 'results/batch5-2017-06-05T07:58:47-best'
    self.grid_spacing = 1.0
    self.max_dist = 10.0
    self.batch = 0
    self.charge_scaler = 0.425896
    self.output = "predictions.csv"
    self.verbose = False

args = FakeArguments()

# TODO: avarage prediction for different rotations (optional)

featurizer = Featurizer()

charge_column = featurizer.FEATURE_NAMES.index('partialcharge')

session = tf.Session()

saver = tf.train.import_meta_graph('%s.meta' % args.network,
                                   clear_devices=True)
saver.restore(session, args.network)

predict = tf.get_collection('output')[0]
inp = tf.get_collection('input')[0]
kp = tf.get_collection('kp')[0]


def get_batch(coords, features):

    batch_grid = []

    for crd, f in zip(coords, features):
        batch_grid.append(make_grid(crd, f, max_dist=args.max_dist,
                          grid_resolution=args.grid_spacing))
        if len(batch_grid) == args.batch:
            # if batch is not specified it will never happen
            batch_grid = np.vstack(batch_grid)
            batch_grid[..., charge_column] /= args.charge_scaler
            return batch_grid

    if len(batch_grid) > 0:
        batch_grid = np.vstack(batch_grid)
        batch_grid[..., charge_column] /= args.charge_scaler
        return batch_grid


def predict_affinity(dataset):
    coords = []
    features = []
    coords.append(dataset[:, :3])
    features.append(dataset[:, 3:])

    # predict = tf.get_collection('output')[0]
    # inp = tf.get_collection('input')[0]
    # kp = tf.get_collection('kp')[0]

    # saver.restore(session, args.network)
    batch_grid = get_batch(coords, features)
    prediction = session.run(predict, feed_dict={inp: batch_grid, kp: 1.0})
    return np.vstack(prediction).flatten()[0]

